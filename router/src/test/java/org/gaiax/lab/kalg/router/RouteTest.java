/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.router;

import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.gaiax.lab.kalg.services.policyenforcer.route.PolicyEnforcerRoute;
import org.gaiax.lab.kalg.services.tokenissuer.route.TokenIssuerRoute;
import org.junit.jupiter.api.Test;

import static org.gaiax.lab.kalg.common.Constants.*;

public class RouteTest extends CamelTestSupport {

    @Test
    public void end2end() throws Exception {
        getMockEndpoint("mock:result").expectedBodiesReceived("Hello GAIA-X");

        template.sendBody("direct:start", "dummy");

        assertMockEndpointsSatisfied();
    }

    @Override
    protected RoutesBuilder[] createRouteBuilders() {
        return new RoutesBuilder[]{
                new TokenIssuerRoute(),
                new PolicyEnforcerRoute(),
                new RouteBuilder() {
                    @Override
                    public void configure() throws Exception {
                        from("direct:start")
                                .setBody(constant("Hello GAIA-X"))
                                .setHeader(SOURCE_ORG_SD_HEADER, constant("file:target/source_org_sd.json"))
                                .setHeader(SINK_ORG_SD_HEADER, constant("file:target/sink_org_sd.json"))
                                .setHeader(DATA_EXCHANGE_SD_HEADER, constant("file:target/data_exchange_sd.json"))
                                .to("direct-vm:gaia-x-token-issuer")
                                .to("log:gaia-x")
                                .to("direct-vm:gaia-x-policy-enforcer")
                                .to("mock:result");
                    }
                }
        };
    }

}

# GAIA-X Lab :: Kalg

GAIA-X Lab Kalg project is PoC showing data exchange between organization compliant with the GAIA-X ecosystem.

The data scientist writes the data exchange routing using a dedicated DSL.

## Kalg Framework

Kalg framework is based on [Apache Camel](https://camel.apache.org).

On top of Apache Camel features (existing components, EIPs, languages, DSL, ...), Kalg adds:
* a `gx` (e.g. GAIA-X) component that wraps existing Camel components adding GAIA-X required metadata, e.g. GAIA-X compliance self-description
* a set of processors: access control processor, usage policy control processor, ...
* GAIA-X specific DSL for data exchange routing

The different Camel layers are packaged all together in Kalg framework.

## Kalg Runtime

Kalg runtime include the framework and actually run the routing engine with the descriptors provided by the data scientist.

This runtime is powered by [Apache Karaf](https://karaf.apache.org), provising a modulith runtime that can run on premise or on cloud.

## Getting started

### Bootstrap

To use Klag framework, you just have to add klag jar in your classpath.

Basically, if you use Maven, you just define klag framwork as dependency:


```
<dependency>
  <groupId>org.gaiax.lab.klag</groupId>
  <artifactId>framework</artifactId>
  <version>RELEASE</version>
</dependency>
```

### Explicit

You can use Kalg DSL explicitely calling the Kalg processor which orchestrate all internal control processors:

```
from("hdfs:/path").process(Kalg.class).to("...").to("...").to("kafka:topic")
```

### Implicit

You can also use Kalg DSL with `gx` implicit component, wrapping concrete component. This component automatically validate data/message and call control processors:

```
from("hdfs:/path").to("...").to("gx:kafka:topic")
```

## Contact us

/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.tokenissuer.processor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.gaiax.lab.kalg.common.Constants.SOURCE_ORG_SD_HEADER;
import static org.gaiax.lab.kalg.common.Constants.TOKEN_HEADER;

public class TokenIssuerProcessor implements Processor {

    private final static Logger LOGGER = LoggerFactory.getLogger(TokenIssuerProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("Check if the {} header is present in Exchange Message", SOURCE_ORG_SD_HEADER);
        if (exchange.getMessage().getHeader(SOURCE_ORG_SD_HEADER) == null) {
            throw new IllegalArgumentException(SOURCE_ORG_SD_HEADER + " header is not present");
        }
        // TODO retrieve source organization self descriptor, verify it and use it to generate token
        LOGGER.debug("Verify source organization self descriptor");
        LOGGER.debug("Generate JWT");
        Algorithm algorithm = Algorithm.HMAC256("secret");
        String token = JWT.create()
                .withIssuer("gaia-x")
                .sign(algorithm);
        exchange.getMessage().setHeader(TOKEN_HEADER, token);
    }

}

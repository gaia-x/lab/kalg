/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.tokenissuer.route;

import org.apache.camel.RoutesBuilder;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.junit.jupiter.api.Test;

import static org.gaiax.lab.kalg.common.Constants.SOURCE_ORG_SD_HEADER;
import static org.gaiax.lab.kalg.common.Constants.TOKEN_HEADER;

public class TokenIssuerRouteTest extends CamelTestSupport {

    @Test
    public void normalFlow() throws Exception {
        getMockEndpoint("mock:log:gaia-x").expectedBodiesReceived("Hello GAIA-X");
        getMockEndpoint("mock:log:gaia-x").expectedHeaderReceived(TOKEN_HEADER, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnYWlhLXgifQ.dTuIHIqVScIk-ydCxT4Tdke_Axk4wHaeul8lZF1Hlq8");

        template.sendBodyAndHeader("direct-vm:gaia-x-token-issuer", "Hello GAIA-X", SOURCE_ORG_SD_HEADER, "foo");

        assertMockEndpointsSatisfied();
    }


    @Override
    protected RoutesBuilder createRouteBuilder() {
        return new TokenIssuerRoute();
    }

    @Override
    public String isMockEndpoints() {
        return "log:(.*)";
    }

}

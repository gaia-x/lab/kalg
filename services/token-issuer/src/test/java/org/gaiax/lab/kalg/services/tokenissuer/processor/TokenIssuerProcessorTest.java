/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.tokenissuer.processor;

import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.gaiax.lab.kalg.common.Constants.SOURCE_ORG_SD_HEADER;
import static org.gaiax.lab.kalg.common.Constants.TOKEN_HEADER;

public class TokenIssuerProcessorTest {

    @Test
    public void noHeader() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        TokenIssuerProcessor processor = new TokenIssuerProcessor();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            processor.process(exchange);
        });
        Assertions.assertEquals(SOURCE_ORG_SD_HEADER + " header is not present", exception.getMessage());
    }

    @Test
    public void happyPath() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);
        exchange.getMessage().setHeader(SOURCE_ORG_SD_HEADER, "http://foo.bar");

        TokenIssuerProcessor processor = new TokenIssuerProcessor();
        processor.process(exchange);

        Assertions.assertNotNull(exchange.getMessage().getHeader(TOKEN_HEADER));
    }

}

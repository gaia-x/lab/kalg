/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.common;

import org.gaiax.lab.kalg.common.model.SelfDescriptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.net.URL;

public class SelfDescriptorParserTest {

    @Test
    public void simpleLoadViaStream() throws Exception {
        SelfDescriptor selfDescriptor = SelfDescriptorParser.load(new FileInputStream("target/test-classes/simple.json"));

        Assertions.assertNotNull(selfDescriptor.getAttributes());
        Assertions.assertNotNull(selfDescriptor.getRules());

        Assertions.assertEquals("bar", selfDescriptor.getAttributes().get("foo"));
        Assertions.assertEquals(1, selfDescriptor.getAttributes().get("number"));
        Assertions.assertEquals(2.0, selfDescriptor.getAttributes().get("double"));

        Assertions.assertEquals("foo:bar AND number > 0", selfDescriptor.getRules().get("first"));
        Assertions.assertEquals("fromindex:foo* OR range:[0 TO 200]", selfDescriptor.getRules().get("second"));
    }

    @Test
    public void simpleLoadViaUrl() throws Exception {
        SelfDescriptor selfDescriptor = SelfDescriptorParser.load(new URL("file:target/test-classes/simple.json"));

        Assertions.assertNotNull(selfDescriptor.getAttributes());
        Assertions.assertNotNull(selfDescriptor.getRules());

        Assertions.assertEquals("bar", selfDescriptor.getAttributes().get("foo"));
        Assertions.assertEquals(1, selfDescriptor.getAttributes().get("number"));
        Assertions.assertEquals(2.0, selfDescriptor.getAttributes().get("double"));

        Assertions.assertEquals("foo:bar AND number > 0", selfDescriptor.getRules().get("first"));
        Assertions.assertEquals("fromindex:foo* OR range:[0 TO 200]", selfDescriptor.getRules().get("second"));
    }

}

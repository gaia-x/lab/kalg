/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.common;

public class Constants {

    public final static String SOURCE_ORG_SD_HEADER = "gaia-x.source.organization.self.descriptor";
    public final static String SINK_ORG_SD_HEADER = "gaia-x.sink.organization.self.descriptor";
    public final static String DATA_EXCHANGE_SD_HEADER = "gaia-x.data.exchange.self.descriptor";
    public final static String TOKEN_HEADER = "gaia-x.token";

}

/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.policyenforcer.processor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.gaiax.lab.kalg.common.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PolicyEnforcerProcessor implements Processor {

    private final static Logger LOGGER = LoggerFactory.getLogger(PolicyEnforcerProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("Verify presence of the source, sink, data exchange SD headers");
        if (exchange.getMessage().getHeader(Constants.SOURCE_ORG_SD_HEADER) == null) {
            throw new IllegalStateException(Constants.SOURCE_ORG_SD_HEADER + " header is not present");
        }
        if (exchange.getMessage().getHeader(Constants.SINK_ORG_SD_HEADER) == null) {
            throw new IllegalStateException(Constants.SINK_ORG_SD_HEADER + " header is not present");
        }
        if (exchange.getMessage().getHeader(Constants.DATA_EXCHANGE_SD_HEADER) == null) {
            throw new IllegalStateException(Constants.DATA_EXCHANGE_SD_HEADER + " header is not present");
        }
        LOGGER.debug("Verify JWT");
        if (exchange.getMessage().getHeader(Constants.TOKEN_HEADER) == null) {
            throw new IllegalStateException(Constants.TOKEN_HEADER + " header is not present");
        }
        Algorithm algorithm = Algorithm.HMAC256("secret");
        JWTVerifier verifier = JWT.require(algorithm).withIssuer("gaia-x").build();
        DecodedJWT jwt = verifier.verify(exchange.getMessage().getHeader(Constants.TOKEN_HEADER, String.class));
        // TODO call policy rule engine
    }

}

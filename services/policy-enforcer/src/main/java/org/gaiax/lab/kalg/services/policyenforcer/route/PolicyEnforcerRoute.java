/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.policyenforcer.route;

import org.apache.camel.builder.RouteBuilder;
import org.gaiax.lab.kalg.services.policyenforcer.processor.PolicyEnforcerProcessor;

public class PolicyEnforcerRoute extends RouteBuilder {

    @Override
    public void configure() {
        from("direct-vm:gaia-x-policy-enforcer")
                .log("Verifying GAIA-X policy rules")
                .process(new PolicyEnforcerProcessor())
                .to("log:gaia-x");
    }

}

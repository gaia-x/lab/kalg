/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.policyenforcer.processor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.gaiax.lab.kalg.common.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PolicyEnforcerProcessorTest {

    @Test
    public void noSourceOrgHeader() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        PolicyEnforcerProcessor processor = new PolicyEnforcerProcessor();
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> {
           processor.process(exchange);
        });
        Assertions.assertEquals(Constants.SOURCE_ORG_SD_HEADER + " header is not present", exception.getMessage());
    }

    @Test
    public void noSinkOrgHeader() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        exchange.getMessage().setHeader(Constants.SOURCE_ORG_SD_HEADER, "source");

        PolicyEnforcerProcessor processor = new PolicyEnforcerProcessor();
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> {
           processor.process(exchange);
        });
        Assertions.assertEquals(Constants.SINK_ORG_SD_HEADER + " header is not present", exception.getMessage());
    }

    @Test
    public void noDataExchangeHeader() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        exchange.getMessage().setHeader(Constants.SOURCE_ORG_SD_HEADER, "source");
        exchange.getMessage().setHeader(Constants.SINK_ORG_SD_HEADER, "sink");

        PolicyEnforcerProcessor processor = new PolicyEnforcerProcessor();
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> {
           processor.process(exchange);
        });
        Assertions.assertEquals(Constants.DATA_EXCHANGE_SD_HEADER + " header is not present", exception.getMessage());
    }

    @Test
    public void badToken() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        exchange.getMessage().setHeader(Constants.SOURCE_ORG_SD_HEADER, "source");
        exchange.getMessage().setHeader(Constants.SINK_ORG_SD_HEADER, "sink");
        exchange.getMessage().setHeader(Constants.DATA_EXCHANGE_SD_HEADER, "data-exchange");
        exchange.getMessage().setHeader(Constants.TOKEN_HEADER, "xxxx.yyyy.zzzz");

        PolicyEnforcerProcessor processor = new PolicyEnforcerProcessor();
        JWTDecodeException exception = Assertions.assertThrows(JWTDecodeException.class, () -> {
            processor.process(exchange);
        });
    }

    @Test
    public void happyPath() throws Exception {
        DefaultCamelContext camelContext = new DefaultCamelContext();
        DefaultExchange exchange = new DefaultExchange(camelContext);

        exchange.getMessage().setHeader(Constants.SOURCE_ORG_SD_HEADER, "source");
        exchange.getMessage().setHeader(Constants.SINK_ORG_SD_HEADER, "sink");
        exchange.getMessage().setHeader(Constants.DATA_EXCHANGE_SD_HEADER, "data-exchange");

        Algorithm algorithm = Algorithm.HMAC256("secret");
        String token = JWT.create()
                .withIssuer("gaia-x")
                .sign(algorithm);

        exchange.getMessage().setHeader(Constants.TOKEN_HEADER, token);

        PolicyEnforcerProcessor processor = new PolicyEnforcerProcessor();
        processor.process(exchange);
    }

}

/*
 * Copyright 2022 GAIA-X Lab.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaiax.lab.kalg.services.policyenforcer.route;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.gaiax.lab.kalg.common.Constants;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.gaiax.lab.kalg.common.Constants.*;

public class PolicyEnforcerRouteTest extends CamelTestSupport {

    @Test
    public void normalFlow() throws Exception {
        getMockEndpoint("mock:log:gaia-x").expectedBodiesReceived("Hello GAIA-X");
        getMockEndpoint("mock:log:gaia-x").expectedHeaderReceived(TOKEN_HEADER, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnYWlhLXgifQ.dTuIHIqVScIk-ydCxT4Tdke_Axk4wHaeul8lZF1Hlq8");

        Map<String, Object> headers = new HashMap<>();
        headers.put(SOURCE_ORG_SD_HEADER, "source");
        headers.put(SINK_ORG_SD_HEADER, "sink");
        headers.put(DATA_EXCHANGE_SD_HEADER, "data-exchange");

        Algorithm algorithm = Algorithm.HMAC256("secret");
        String token = JWT.create()
                .withIssuer("gaia-x")
                .sign(algorithm);

        headers.put(TOKEN_HEADER, token);
        template.sendBodyAndHeaders("direct-vm:gaia-x-policy-enforcer", "Hello GAIA-X", headers);

        assertMockEndpointsSatisfied();
    }


    @Override
    protected RoutesBuilder createRouteBuilder() {
        return new PolicyEnforcerRoute();
    }

    @Override
    public String isMockEndpoints() {
        return "log:(.*)";
    }

}
